var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
//barva default
$(document).ready(function() {document.querySelector("#dobiBMI").style.backgroundColor="lightgreen";});
$(document).ready(function() {document.querySelector("#dobiTelTemp").style.backgroundColor="lightgreen";});
$(document).ready(function() {document.querySelector("#dobiTlakSistol").style.backgroundColor="lightgreen";});
$(document).ready(function() {document.querySelector("#dobiTlakDiastol").style.backgroundColor="lightgreen";});
$(document).ready(function() {document.querySelector("#dobiKisikKri").style.backgroundColor="lightgreen";});


//Spremeni barvo
function barvaBMI() {
    document.querySelector("#dobiBMI").style.backgroundColor="red";
}
function barvaTemp() {
    document.querySelector("#dobiTelTemp").style.backgroundColor="red";
}
function barvaTlakSistol() {
    document.querySelector("#dobiTlakSistol").style.backgroundColor="red";
}
function barvaTlakDias() {
    document.querySelector("#dobiTlakDiastol").style.backgroundColor="red";
}
function barvaKisik() {
    document.querySelector("#dobiKisikKri").style.backgroundColor="red";
}

//Preberi polje 
function vnosVitalnihZnakov() {
    var telVisina = document.querySelector("#dodajVitalnoTelesnaVisina").value;
    var telTeza = document.querySelector("#dodajVitalnoTelesnaTeza").value;
    var telTemp = document.querySelector("#dodajVitalnoTelesnaTemperatura").value;
    var tlakSistol = document.querySelector("#dodajVitalnoKrvniTlakSistolicni").value;
    var tlakDias = document.querySelector("#dodajVitalnoKrvniTlakDiastolicni").value;
    var kisikKri = document.querySelector("#dodajVitalnoNasicenostKrviSKisikom").value;
    var BMI = (telTeza/(telVisina*telVisina))*10000;
  
    document.querySelector("#dobiBMI").value=BMI;
    if(BMI>30 || BMI<20) {
        barvaBMI();
    }else {
       $(document).ready(function() {document.querySelector("#dobiBMI").style.backgroundColor="lightgreen";}); 
    }
    
    document.querySelector("#dobiTelTemp").value=telTemp;
    if(telTemp>37 || telTemp<35) {
        barvaTemp();
    }else {
        $(document).ready(function() {document.querySelector("#dobiTelTemp").style.backgroundColor="lightgreen";});
    }
    
    document.querySelector("#dobiTlakSistol").value=tlakSistol;
    if(tlakSistol<80 || tlakSistol>140) {
        barvaTlakSistol();
    }else {
        $(document).ready(function() {document.querySelector("#dobiTlakSistol").style.backgroundColor="lightgreen";});
    }
    document.querySelector("#dobiTlakDiastol").value=tlakDias;
    if(tlakDias<60 || tlakDias>100) {
        barvaTlakDias();
    }else {
        $(document).ready(function() {document.querySelector("#dobiTlakDiastol").style.backgroundColor="lightgreen";});
    }
    document.querySelector("#dobiKisikKri").value=kisikKri;
    if(kisikKri<95) {
        barvaKisik();
    }else {
        $(document).ready(function() {document.querySelector("#dobiKisikKri").style.backgroundColor="lightgreen";});
    }
}